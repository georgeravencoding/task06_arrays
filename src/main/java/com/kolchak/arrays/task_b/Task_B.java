package com.kolchak.arrays.task_b;

import org.apache.logging.log4j.*;

import java.util.*;


public class Task_B {
    public static Logger logger = LogManager.getLogger(Task_B.class);

    public static void main(String[] args) {
        logger.info("Remove all values from array which are duplicated");
        Integer[] mas1 = {1, 1, 3, 2, 3, 2, 5, 5, 5};
        int s = mas1.length;
        for (int i = 0; i < s - 1; i++) {
            for (int j = i + 1; j < mas1.length; j++) {
                if (mas1[i] == mas1[j]) {
                    mas1[i] = 0;
                }
            }
        }
        logger.info("Easy way:");
        Integer[] mas2 = {1, 1, 3, 2, 3, 2, 5, 5, 5};
        Set<Integer> integerList = new HashSet<>();
        for (int i = 0; i < mas2.length; i++) {
            integerList.add(mas2[i]);
        }
        logger.info(integerList);
    }
}
