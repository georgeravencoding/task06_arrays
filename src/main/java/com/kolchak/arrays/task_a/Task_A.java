package com.kolchak.arrays.task_a;

import org.apache.logging.log4j.*;

import java.util.ArrayList;
import java.util.List;

public class Task_A {
    public static Logger logger = LogManager.getLogger(Task_A.class);

    public static void main(String[] args) {
        logger.info("Task A");
        int[] mas1 = new int[]{1, 5, 3, 4, 9, 6};
        int[] mas2 = new int[]{6, 1, 2, 7, 8, 4};

        boolean contains = false;

        List<Integer> masUnique = new ArrayList<Integer>();
        List<Integer> masCommon = new ArrayList<Integer>();

        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas2.length; j++) {
                if (mas1[i] == mas2[j]) {
                    contains = true;
                    break;
                }
            }

            if (!contains) {
                masUnique.add(mas1[i]);
            } else {
                contains = false;
                masCommon.add(mas1[i]);
            }
        }
        logger.info("Unique values: " + masUnique);
        logger.info("Common values: " + masCommon);

    }
}

