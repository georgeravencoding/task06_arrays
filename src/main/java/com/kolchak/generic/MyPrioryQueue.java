package com.kolchak.generic;

import java.util.*;

public class MyPrioryQueue<T extends Comparable> implements Queue {
    private List<T> queue;
    private Comparator<T> comparator;

    public MyPrioryQueue(Comparator<T> comparator) {
        queue = new ArrayList<T>();
        this.comparator = comparator;
    }

    @Override
    public boolean add(Object o) {
        queue.add((T) o);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int i = queue.indexOf(o);
        if (i < 0) {
            return false;
        } else {
            queue.remove(o);
            return true;
        }
    }

    @Override
    public Object poll() {
        if (size() == 0) return null;
        int headNumber = findHeadNumber();
        Object obj = queue.get(headNumber);
        queue.remove(headNumber);
        return obj;
    }

    @Override
    public Object peek() {
        return queue.get(findHeadNumber());
    }

    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public Iterator iterator() {
        for (T t : queue) {
            Main.logger.info(t);
        }
        return null;
    }

    private int findHeadNumber() {
        int headNumber = 0;
        for (int i = 0; i < size(); i++) {
            if (comparator.compare(queue.get(i), queue.get(headNumber)) > 0) headNumber = i;
        }
        return headNumber;
    }

    @Override
    public boolean isEmpty() {
        if (queue.size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }


    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public Object remove() {
        return null;
    }


    @Override
    public Object element() {
        return null;
    }

}
