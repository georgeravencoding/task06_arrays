package com.kolchak.generic;

import java.util.List;

public class Container<T extends Furniture> {


    private T containerWithFurniture;

    public T getUnitInContainer() {
        return containerWithFurniture;
    }

    public void putUnitInContainer(T containerWithFurniture) {
        this.containerWithFurniture = containerWithFurniture;
    }

    public static void showUnits(List<? extends Furniture> units) {
        for (Furniture unit : units) {
            Main.logger.info(unit);
        }
    }
}
