package com.kolchak.generic;

public class Window extends Furniture {
    public Window(String material) {
        super(material);
    }

    @Override
    public String toString() {
        return "Window{" +
                "material='" + material + '\'' +
                "} " + super.toString();
    }
}
