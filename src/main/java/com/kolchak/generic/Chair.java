package com.kolchak.generic;

public class Chair extends Furniture {
    public Chair(String material) {
        super(material);
    }

    @Override
    public String toString() {
        return "Chair{" +
                "material='" + material + '\'' +
                "} " + super.toString();
    }
}
