package com.kolchak.generic;

import org.apache.logging.log4j.*;

import java.util.*;

public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Create Priory Queue");
        Queue<Integer> integerMyPrioryQueue = new MyPrioryQueue<Integer>(Integer::compareTo);
        integerMyPrioryQueue.add(2);
        integerMyPrioryQueue.add(4);
        integerMyPrioryQueue.add(55);
        integerMyPrioryQueue.add(34);
        integerMyPrioryQueue.add(27);
        integerMyPrioryQueue.add(26);
        integerMyPrioryQueue.add(8);

        while (!integerMyPrioryQueue.isEmpty()) {
            logger.info(integerMyPrioryQueue.poll());
        }
        logger.info("Generic container with units");

        List<Furniture> furnitures = new ArrayList<>();
        furnitures.add(new Chair("Plastic"));
        furnitures.add(new Table("Wooden"));
        furnitures.add(new Window("Glass"));

        Container.showUnits(furnitures);

        logger.info("Add string into List<Integers>");

//        List<Integer> integerList = new ArrayList<>();
//        integerList.add("kshdkfh");
    }
}
