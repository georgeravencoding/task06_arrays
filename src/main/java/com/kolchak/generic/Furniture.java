package com.kolchak.generic;

public class Furniture {
    protected String material;

    public Furniture(String material) {
        this.material = material;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "material='" + material + '\'' +
                '}';
    }
}
