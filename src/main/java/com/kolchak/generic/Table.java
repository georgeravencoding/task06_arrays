package com.kolchak.generic;

public class Table extends Furniture {
    public Table(String material) {
        super(material);
    }

    @Override
    public String toString() {
        return "Table{" +
                "material='" + material + '\'' +
                "} " + super.toString();
    }
}
